package dataBase;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

public class SingleSession {

    private static SingleSession instance = new SingleSession();
    private SessionFactory sessionFactory;

    private SingleSession() {
        
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
    }

    public static SingleSession getInstance() {
        return instance;
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }
    public void closeFactory() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}