/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entitats;

import jakarta.persistence.Entity;
import java.sql.Date;

/**
 *
 * @author Ruben
 */
public class Employee {
    private int id;
    private String nom;
    private int antiguetat;
    private java.sql.Date fecha;

    public Employee(){
        
    }
    public Employee(int id,String nom,int antiguetat,Date fecha){
        this.id = id;
        this.nom = nom;
        this.antiguetat = antiguetat;
        this.fecha = fecha;
    }
    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the antiguetat
     */
    public int getAntiguetat() {
        return antiguetat;
    }

    /**
     * @param antiguetat the antiguetat to set
     */
    public void setAntiguetat(int antiguetat) {
        this.antiguetat = antiguetat;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
