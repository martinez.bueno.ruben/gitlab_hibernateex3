/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.github.javafaker.Faker;
import dataBase.SingleSession;
import entitats.Employee;
import java.sql.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author Ruben
 */
public class Main {

    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {
        try{
        Session sesion = SingleSession.getInstance().getSession();
        crearRegistros(sesion);
        mostrarEmpleats(sesion);
        interaccioEmpleats(sesion);
        }
        catch(InterruptedException e){
            log.info(e.getMessage());
        }
    }

    public static void crearRegistros(Session sesion) throws InterruptedException {
        sesion.getTransaction().begin();
        int comptar = 0;
        for (int i = 0; i < 1000; i++) {
            Employee empleado = new Employee();
            Faker faker = new Faker();
            String nombre = faker.name().name();
            String antiguetat = faker.numerify("##");
            java.util.Date fechaFaker = faker.date().birthday(17, 75);
            java.sql.Date fecha = new java.sql.Date(fechaFaker.getTime());
            empleado.setNom(nombre);
            empleado.setAntiguetat(Integer.parseInt(antiguetat));
            empleado.setFecha(fecha);
            comptar++;
            sesion.persist(empleado);
        }
        sesion.getTransaction().commit();
    }

    public static void mostrarEmpleats(Session sesion) throws InterruptedException {
        long milisegons = System.currentTimeMillis();
        Date fechaActual = new Date(milisegons);
        Query<Employee> query = sesion.createQuery("FROM Employee", Employee.class);
        List<Employee> listaEmpleados = query.list();
        for (int i = 0; i < listaEmpleados.size(); i++) {
            int años = fechaActual.getYear() - listaEmpleados.get(i).getFecha().getYear();
            log.info("Nom: " + listaEmpleados.get(i).getNom() + " antiguetat: " + listaEmpleados.get(i).getAntiguetat() + " fecha: " + listaEmpleados.get(i).getFecha() + " años: " + años);
        }
        Query query2 = sesion.createQuery("SELECT COUNT(*) FROM Employee", Long.class);
        long total = (long) query2.uniqueResult();
        log.info("Total de empleats insertats: " + total);
        Thread.sleep(5000);
    }

    public static void interaccioEmpleats(Session sesion) throws InterruptedException {
        long milisegons = System.currentTimeMillis();
        Date fechaActual = new Date(milisegons);
        Query dadesEmpleats = sesion.createQuery("FROM Employee", Employee.class);
        List<Employee> listarEmpleados = dadesEmpleats.list();
        sesion.getTransaction().begin();
        for (Employee Empleado : listarEmpleados) {
            int añoEmpleado = fechaActual.getYear()-Empleado.getFecha().getYear();
            int id = Empleado.getId();
            if (añoEmpleado > 15 && añoEmpleado < 19) {
                Empleado.setNom(Empleado.getNom() + " Junior");
                log.info("Empleado: " + id + " ha sido actualizado");
            }
            else if(añoEmpleado > 50 && añoEmpleado < 66){
                Empleado.setNom(Empleado.getNom() + " Senior");
                log.info("Empleado: " + id + " ha sido actualizado");
            }
            else if(añoEmpleado > 65){
                sesion.remove(Empleado);
                log.info("Se ha eliminado el usuario: " + id);
            }
        }
        sesion.getTransaction().commit();
        Query query2 = sesion.createQuery("SELECT COUNT(*) FROM Employee", Long.class);
        long total = (long) query2.uniqueResult();
        log.info("Total de empleats restants: " + total);
        Thread.sleep(5000);
    }

}
